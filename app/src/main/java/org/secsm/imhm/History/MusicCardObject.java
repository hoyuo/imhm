package org.secsm.imhm.History;

public class MusicCardObject {
    private int album_cover;
    private String music_name;
    private String music_singer;
    private String album_name;
    private double latitude;
    private double longitude;

    public MusicCardObject(int album_cover, String music_name, String music_singer, String album_name, double latitude, double longitude) {
        this.album_cover = album_cover;
        this.music_name = music_name;
        this.music_singer = music_singer;
        this.album_name = album_name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getAlbum_cover() {
        return album_cover;
    }

    public void setAlbum_cover(int album_cover) {
        this.album_cover = album_cover;
    }

    public String getMusic_name() {
        return music_name;
    }

    public void setMusic_name(String music_name) {
        this.music_name = music_name;
    }

    public String getMusic_singer() {
        return music_singer;
    }

    public void setMusic_singer(String music_singer) {
        this.music_singer = music_singer;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
