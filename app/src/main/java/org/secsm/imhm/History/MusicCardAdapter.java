package org.secsm.imhm.History;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.secsm.imhm.Develop;
import org.secsm.imhm.R;

import java.util.ArrayList;

public class MusicCardAdapter extends RecyclerView
        .Adapter<MusicCardAdapter.MusicCardViewHolder> {

    private static String LOG_TAG = MusicCardAdapter.class.getSimpleName();
    private ArrayList<MusicCardObject> mDataset;
    private static MyClickListener myClickListener;
    private Context ctx;

    public void updateData(ArrayList<MusicCardObject> viewModels) {
        mDataset.clear();
        mDataset.addAll(viewModels);
        notifyDataSetChanged();
    }

    public static class MusicCardViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        ImageView album_cover;
        TextView music_name;
        TextView music_singer;
        TextView album_name;

        public MusicCardViewHolder(View itemView) {
            super(itemView);
            album_cover = (ImageView) itemView.findViewById(R.id.album_cover);
            music_name = (TextView) itemView.findViewById(R.id.music_name);
            music_singer = (TextView) itemView.findViewById(R.id.music_singer);
            album_name = (TextView) itemView.findViewById(R.id.album_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }

    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public MusicCardAdapter(ArrayList<MusicCardObject> myDataset, Context context) {
        mDataset = myDataset;
        ctx = context;
    }

    @Override
    public MusicCardViewHolder onCreateViewHolder(ViewGroup parent,
                                                  int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.music_card, parent, false);

        return new MusicCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MusicCardViewHolder holder, int position) {
        holder.music_name.setText(mDataset.get(position).getMusic_name());
        holder.music_singer.setText(mDataset.get(position).getMusic_singer());
        holder.album_name.setText(mDataset.get(position).getAlbum_name());
        Glide.with(ctx)
                .load(Develop.WEB_SERVER + Develop.WEB_SERVER_IMAGE + mDataset.get(position).getAlbum_cover())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.album_cover);
    }

    public void addItem(MusicCardObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
