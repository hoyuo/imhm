package org.secsm.imhm.History;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.secsm.imhm.DataSet;
import org.secsm.imhm.Develop;
import org.secsm.imhm.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class HistoryFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private MusicCardAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = HistoryFragment.class.getSimpleName();

    private static HistoryFragment instance = new HistoryFragment();

    public static HistoryFragment newInstance() {
        return instance;
    }

    public HistoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_history, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MusicCardAdapter(new ArrayList<MusicCardObject>(), getContext());
        mRecyclerView.setAdapter(mAdapter);

        (new HistoryLoader()).execute();
        return view;
    }

    public class HistoryLoader extends AsyncTask<Void, Void, ArrayList<MusicCardObject>> {
        private final ProgressDialog dialog = new ProgressDialog(getContext());

        @Override
        protected ArrayList<MusicCardObject> doInBackground(Void... params) {
            ArrayList<MusicCardObject> result = new ArrayList<>();

            try {
                URL u = new URL(Develop.WEB_SERVER + Develop.WEB_SERVER_HISTORY);
                String param = "id=" + Develop.getUserId(getContext());
                HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
                // 방식결정
                httpURLConnection.setRequestMethod("POST");
                // 해더 설정
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                // 서버 메세지 수신
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDefaultUseCaches(false);

                // paramter 전달
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(param.getBytes());
                os.flush();
                os.close();

                String buffer;
                String ret = "";
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                while (((buffer = in.readLine()) != null)) {
                    ret += buffer;
                }
                in.close();

                JSONObject jsonObject = new JSONObject(ret);
                int size = jsonObject.getInt("arraysize");

                JSONArray arr = jsonObject.getJSONArray("result");

                for (int i = 0; i < size; i++) {
                    result.add(convertMusicCardObject(arr.getJSONObject(i)));
                }

                return result;

            } catch (Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        private MusicCardObject convertMusicCardObject(JSONObject obj) throws JSONException {
            int album_cover = obj.getInt("musicIdx");
            String music_name = obj.getString("title");
            String album_name = obj.getString("albumname");
            String music_singer = obj.getString("artist");
            double latitude = obj.getDouble("latitude");
            double longitude = obj.getDouble("longitude");

            return new MusicCardObject(album_cover, music_name, music_singer, album_name, latitude, longitude);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Downloading contacts...");
            dialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<MusicCardObject> musicCardObjects) {
            super.onPostExecute(musicCardObjects);
            DataSet.newInstance().setmDataSet(musicCardObjects);
            dialog.dismiss();
            mAdapter.updateData(musicCardObjects);
        }
    }
}
