package org.secsm.imhm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        activity = this;
        Handler h = new Handler();
        h.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent();
                activity.setResult(RESULT_OK, i);

                activity.finish();
            }
        }, 1500);
    }
}
