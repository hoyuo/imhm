package org.secsm.imhm.SignUp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.secsm.imhm.Develop;
import org.secsm.imhm.R;

public class SignupActivity extends AppCompatActivity {
    private WebView mWebView;
    private boolean mFlag = false;
    private Handler mHandler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0)
                    mFlag = false;
            }
        };

        mWebView = (WebView) findViewById(R.id.webview);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(this, "IMHMAndroidApp");
        mWebView.loadUrl(Develop.WEB_SERVER + "/mobile");
        mWebView.setWebViewClient(new WebViewClientClass());
    }

    @JavascriptInterface
    public void isSuccess(final boolean b, final String id) {
        if (b) {
            Intent i = new Intent();
            i.putExtra("id", id);
            this.setResult(RESULT_OK, i);
            this.finish();
        } else {
            Toast.makeText(this, "이미 가입된 E-Mail 입니다.", Toast.LENGTH_SHORT).show();
            mWebView.reload();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return false;
            } else {
                if (!mFlag) {
                    Toast.makeText(this, "'뒤로' 버튼을 한번 더 누르시면 로그인 화면으로 돌아갑니다.", Toast.LENGTH_SHORT).show();
                    mFlag = true;
                    mHandler.sendEmptyMessageDelayed(0, 2000);
                    return false;
                } else {
                    finish();
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
