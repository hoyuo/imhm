package org.secsm.imhm.Service;

public class QuickstartPreferences {

  public static final String REGISTRATION_READY = "registrationReady";
  public static final String REGISTRATION_GENERATING = "registrationGenerating";
  public static final String REGISTRATION_COMPLETE = "registrationComplete";
  public static final String MUSIC_RESULT = "MUSIC_RESULT";
}