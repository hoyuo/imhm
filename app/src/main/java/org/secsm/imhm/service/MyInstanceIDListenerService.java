package org.secsm.imhm.Service;

import com.google.android.gms.iid.InstanceIDListenerService;

import android.content.Intent;

public class MyInstanceIDListenerService extends InstanceIDListenerService {

  private static final String LOG_TAG = MyInstanceIDListenerService.class.getSimpleName();

  @Override
  public void onTokenRefresh() {
    Intent intent = new Intent(this, RegistrationIntentService.class);
    startService(intent);
  }
}