package org.secsm.imhm.Service;

import com.google.android.gms.gcm.GcmListenerService;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class MyGcmListenerService extends GcmListenerService {

  private static final String LOG_TAG = MyGcmListenerService.class.getSimpleName();

  @Override
  public void onMessageReceived(String from, Bundle data) {
    String status = data.getString("status");
    String idx = data.getString("idx");

    Log.d(LOG_TAG, "From: " + from);
    Log.d(LOG_TAG, "Title: " + status + " Idx : " + idx);

    localBroad(status, idx);
  }

  private void localBroad(String status, String idx) {
    Intent i = new Intent(QuickstartPreferences.MUSIC_RESULT);
    i.putExtra("musicIdx", Integer.parseInt(idx));
    i.putExtra("status", status);
    LocalBroadcastManager.getInstance(this).sendBroadcast(i);
//    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(QuickstartPreferences.MUSIC_RESULT));
  }
}