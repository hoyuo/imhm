package org.secsm.imhm.Location;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.secsm.imhm.DataSet;
import org.secsm.imhm.Develop;
import org.secsm.imhm.History.MusicCardObject;
import org.secsm.imhm.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;

    private static MapsFragment instance = new MapsFragment();

    public static MapsFragment newInstance() {
        return instance;
    }

    public MapsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_maps, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map_view);
        mapFragment.getMapAsync(this);


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        LatLng pt = new LatLng(37.5626986, 126.9818816);
        CameraPosition cp = new CameraPosition.Builder().target((pt)).zoom(13).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cp));

        if (DataSet.newInstance().count() == 0) {
            (new ServerLoader()).execute();
        }

    }

    public class ServerLoader extends AsyncTask<Void, Void, Void> {
        private final ProgressDialog dialog = new ProgressDialog(getContext());
        private DataSet dataSet;

        @Override
        protected Void doInBackground(Void... params) {

            try {
                URL u = new URL(Develop.WEB_SERVER + Develop.WEB_SERVER_HISTORY);
                String param = "id=" + Develop.getUserId(getContext());
                HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
                // 방식결정
                httpURLConnection.setRequestMethod("POST");
                // 해더 설정
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                // 서버 메세지 수신
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDefaultUseCaches(false);

                // paramter 전달
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(param.getBytes());
                os.flush();
                os.close();

                String buffer;
                String ret = "";
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                while (((buffer = in.readLine()) != null)) {
                    ret += buffer;
                }
                in.close();

                JSONObject jsonObject = new JSONObject(ret);
                int size = jsonObject.getInt("arraysize");

                JSONArray arr = jsonObject.getJSONArray("result");

                for (int i = 0; i < size; i++) {
                    dataSet.addItem(convertMusicCardObject(arr.getJSONObject(i)));
                }

            } catch (Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        private MusicCardObject convertMusicCardObject(JSONObject obj) throws JSONException {
            int album_cover = obj.getInt("musicIdx");
            String music_name = obj.getString("title");
            String album_name = obj.getString("albumname");
            String music_singer = obj.getString("artist");
            double latitude = obj.getDouble("latitude");
            double longitude = obj.getDouble("longitude");

            return new MusicCardObject(album_cover, music_name, music_singer, album_name, latitude, longitude);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Downloading contacts...");
            dialog.show();
            dataSet = DataSet.newInstance();
            dataSet.clear();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            for (MusicCardObject card : dataSet.getmDataSet()) {
                LatLng latLng = new LatLng(card.getLatitude(), card.getLongitude());
                mMap.addMarker(new MarkerOptions().position(latLng).title(card.getMusic_singer() + "-" + card.getMusic_name()).snippet(card.getAlbum_name()));
            }
        }
    }
}
