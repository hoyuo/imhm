package org.secsm.imhm;

import org.secsm.imhm.History.MusicCardObject;

import java.util.ArrayList;

public class DataSet {
    private ArrayList<MusicCardObject> mDataSet = null;
    private static DataSet instance = new DataSet();

    public static DataSet newInstance() {
        return instance;
    }

    private DataSet() {
        mDataSet = new ArrayList<>();
    }

    public void addItem(MusicCardObject object) {
        mDataSet.add(object);
    }

    public void addItem(MusicCardObject object, int index) {
        mDataSet.add(index, object);
    }

    public void clear() {
        if (mDataSet == null) {
            mDataSet = new ArrayList<>();
        } else {
            mDataSet.clear();
        }
    }

    public void removeItem(int position) {
        mDataSet.remove(position);
    }

    public void setmDataSet(ArrayList<MusicCardObject> mDataSet) {
        this.mDataSet = mDataSet;
    }

    public ArrayList<MusicCardObject> getmDataSet() {
        return mDataSet;
    }

    public int count() {
        return mDataSet.size();
    }
}
