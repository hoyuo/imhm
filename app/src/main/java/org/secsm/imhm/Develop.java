package org.secsm.imhm;

import android.content.Context;
import android.content.SharedPreferences;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Develop {
    public static final String WEB_SERVER = "http://210.118.74.57:8088";
    public static final String WEB_SERVER_IMAGE = "/loadimage?idx=";
    public static final String WEB_SERVER_HISTORY = "/mobile/loadhistory";
    public static final String WEB_SERVER_SEARCH = "/mobile/searchmusic?musicidx=";
    public static final String WEB_SERVER_UPLOAD = "/mobile/uploadhistory";
    public static final String WEB_SERVER_UUID = "/mobile/updateuuid";
    public static final String WEB_SERVER_MUSIC = "http://evancho.ery.wo.tc:28273/search/push_raw/";
    public static final String WEB_SERVER_END = "http://evancho.ery.wo.tc:28273/search/end/";

    public static int LIMIT_TIME = 29;

    public static void setTime(int t) {
        LIMIT_TIME = t;
    }

    public static String SHA256(String str) {
        String SHA = "";
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(str.getBytes());
            byte byteData[] = sh.digest();
            StringBuilder sb = new StringBuilder();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }
            SHA = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            SHA = null;
        }
        return SHA;
    }

    public static String getUserId(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences("PrefID", 0);
        return preferences.getString("id", "");
    }

    public static String getUserNick(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences("PrefID", 0);
        return preferences.getString("nick", "");
    }

    public static void cleanPreferences(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences("PrefID", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("id");
        editor.remove("nick");
        editor.apply();
    }

    public static void savePreferences(Context ctx, String email, String nick) {
        SharedPreferences preferences = ctx.getSharedPreferences("PrefID", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("id", email);
        editor.putString("nick", nick);
        editor.apply();
    }

    public static void setUUID(Context ctx, String uuid) {
        SharedPreferences preferences = ctx.getSharedPreferences("PrefID", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("UUID", uuid);
        editor.apply();
    }

    public static String getUUID(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences("PrefID", 0);
        return preferences.getString("UUID", "");
    }
}
