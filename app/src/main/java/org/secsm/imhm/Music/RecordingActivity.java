package org.secsm.imhm.Music;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.vlonjatg.progressactivity.ProgressActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.secsm.imhm.Develop;
import org.secsm.imhm.Location.GpsInfo;
import org.secsm.imhm.R;
import org.secsm.imhm.Service.QuickstartPreferences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;

public class RecordingActivity extends AppCompatActivity implements View.OnClickListener {
    // TODO 반복 횟수 결정
    final int Limit = Develop.LIMIT_TIME;

    boolean isRecording;

    int frequency = 22050;
    int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
    int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;

    private int mCurRecTimeMs = 0;

    RecordThread recordThread = null;

    short[] data;
    int dataIndex;
    int i;

    String retIdx;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public void registBroadcastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                recordThread.interrupt();
                isRecording = false;
                (new EndTask()).execute();
                stopRec();
                String action = intent.getAction();
                if (action.equals(QuickstartPreferences.MUSIC_RESULT)) {
                    Intent i = new Intent(getApplicationContext(), MusicResultActivity.class);
                    int idx = intent.getIntExtra("musicIdx", 0);
                    if (idx == 0) {
                        Toast.makeText(getApplicationContext(), "Music Search Fail", Toast.LENGTH_LONG).show();
                        finish();
                    }
                    i.putExtra("musicIdx", idx);

                    GpsInfo gps = new GpsInfo(getApplicationContext());
//          GPS 사용유무 가져오기
                    if (gps.isGetLocation()) {
                        i.putExtra("latitude", gps.getLatitude());
                        i.putExtra("longitude", gps.getLongitude());
                    }
                    startActivity(i);
                    finish();
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.MUSIC_RESULT));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording);

        registBroadcastReceiver();

//    Button btn = (Button) findViewById(R.id.recording);
//    btn.setOnClickListener(this);
//    Button btn1 = (Button) findViewById(R.id.recording1);
//    btn1.setOnClickListener(this);

        recordThread = new RecordThread();
        ProgressActivity progressActivity = (ProgressActivity) findViewById(R.id.progressActivity);
        progressActivity.showLoading();

        startRec();
    }

    @Override
    public void onClick(View arg0) {
//    if (arg0.getId() == R.id.recording) {
//      startRec();
//    } else if (arg0.getId() == R.id.recording1) {
//      stopRec();
//    }
    }

    public void startRec() {
        if (data == null)
            data = new short[22050 * 60];
        dataIndex = 0;
        i = 0;
        mCurRecTimeMs = -1000;

        if (recordThread != null) {
            recordThread = null;
            recordThread = new RecordThread();
        }
        recordThread.start();
        isRecording = true;
        handler.sendEmptyMessageDelayed(0, 1000);

        Toast.makeText(this, "start Record", Toast.LENGTH_LONG).show();
    }

    public void stopRec() {
        mCurRecTimeMs = -9999999;
        handler.sendEmptyMessageDelayed(0, 0);
        recordThread.interrupt();
        if (isRecording) {
            (new EndTask()).execute();
            isRecording = false;
        }
    }

    private class Task extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {

            try {
                URL u = new URL(Develop.WEB_SERVER_MUSIC);

                JSONObject param = new JSONObject();
                param.put("username", Develop.getUserId(getApplicationContext()));
                param.put("sequence", params[0]);
                param.put("b64stream", fileToString(params[0]));

                HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
                // 방식결정
                httpURLConnection.setRequestMethod("POST");
                // 해더 설정
                httpURLConnection.setRequestProperty("Content-Type", "application/raw");
                // 서버 메세지 수신
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDefaultUseCaches(false);

                // paramter 전달
                OutputStreamWriter wr = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
                wr.write(param.toString());
                wr.flush();
                wr.close();

                String buffer;
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String ret = "";
                JSONObject obj;
                while (((buffer = in.readLine()) != null)) {
                    ret += buffer;
                }
                obj = new JSONObject(ret);
                retIdx = obj.getString("current_session_idx");
                in.close();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class EndTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            URL u = null;
            try {
                u = new URL(Develop.WEB_SERVER_END + retIdx + "/");

                HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                String buffer;
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                while (((buffer = in.readLine()) != null)) {
                    Log.d("TEST", buffer);
                }
                in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String fileToString(int index) {
        Log.d("Test", index + "");

        String fileString;

        ByteBuffer byteBuffer = ByteBuffer.allocate(frequency * 2);
        for (int i = 0; i < frequency; ++i) {
            byteBuffer.putShort(data[index * frequency + i]);
        }

        byte[] fileArray = byteBuffer.array();
        fileString = Base64.encodeToString(fileArray, Base64.DEFAULT);

        return fileString;
    }

    class RecordThread extends Thread implements Runnable {

        boolean isRecording = false;

        public RecordThread() {
            isRecording = false;
        }

        void stopThread() {
            isRecording = false;
        }

        @Override
        public void run() {
            super.run();

            int bufferSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, audioEncoding);
            AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER, frequency, channelConfiguration, audioEncoding, bufferSize);

            short[] buffer = new short[bufferSize];
            audioRecord.startRecording();
            isRecording = true;
            while (isRecording) {
                int bufferReadResult = audioRecord.read(buffer, 0, bufferSize);
                for (int i = 0; i < bufferReadResult; i++) {
                    data[dataIndex++] = buffer[i];

                    if (dataIndex % frequency == 0) {
                        taskHandler.sendEmptyMessageDelayed(0, 0);
                    }
                }
            }
            audioRecord.stop();

            Log.d("TEST", "녹음종료");
        }
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            mCurRecTimeMs = mCurRecTimeMs + 1000;

            if (mCurRecTimeMs < 0) {
                recordThread.stopThread();
            } else if (mCurRecTimeMs <= Limit * 1000) {
                handler.sendEmptyMessageDelayed(0, 1000);
            } else {
                recordThread.stopThread();
                stopRec();
            }
        }
    };

    Handler taskHandler = new Handler() {
        public void handleMessage(Message msg) {
            (new Task()).execute(i);
            i++;
            Log.d("Test", "SEND");
        }
    };

    @Override
    public void onBackPressed() {
        recordThread.interrupt();
        stopRec();
        super.onBackPressed();
    }
}
