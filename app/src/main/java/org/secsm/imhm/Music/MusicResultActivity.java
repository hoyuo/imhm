package org.secsm.imhm.Music;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;
import org.secsm.imhm.Develop;
import org.secsm.imhm.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MusicResultActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    int musicIdx;
    double latitude;
    double longitude;

    ImageView album_cover;
    TextView music_name;
    TextView music_singer;
    TextView album_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_result);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view);
        mapFragment.getMapAsync(this);

        CardView cardview = (CardView) findViewById(R.id.cardview);
        album_cover = (ImageView) cardview.findViewById(R.id.album_cover);
        music_name = (TextView) cardview.findViewById(R.id.music_name);
        music_singer = (TextView) cardview.findViewById(R.id.music_singer);
        album_name = (TextView) cardview.findViewById(R.id.album_name);

        Intent i = getIntent();
        // TODO 인자 전달 체크
        musicIdx = i.getIntExtra("musicIdx", 634);
        latitude = i.getDoubleExtra("latitude", 126.977958);
        longitude = i.getDoubleExtra("longitude", 37.566615);

        Button btn = (Button) cardview.findViewById(R.id.musicresultBtn);
        btn.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng l = new LatLng(latitude, longitude);
        (new ServerLoader()).execute(musicIdx);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(l, 16));

        googleMap.addMarker(new MarkerOptions()
                .title("검색위치")
                .position(l));
    }

    public class ServerLoader extends AsyncTask<Integer, Void, Void> {
        String ret_music_name;
        String ret_music_singer;
        String ret_album_name;

        ProgressDialog dialog = new ProgressDialog(MusicResultActivity.this);

        @Override
        protected Void doInBackground(Integer... params) {

            try {
                URL u = new URL(Develop.WEB_SERVER + Develop.WEB_SERVER_SEARCH + params[0]);
                HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                String buffer;
                String ret = "";
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                while (((buffer = in.readLine()) != null)) {
                    ret += buffer;
                }
                in.close();

                JSONObject jsonObject = new JSONObject(ret);
                ret_music_name = jsonObject.getString("title");
                ret_album_name = jsonObject.getString("album");
                ret_music_singer = jsonObject.getString("artist");
            } catch (Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading...");
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();

            Glide.with(MusicResultActivity.this)
                    .load(Develop.WEB_SERVER + Develop.WEB_SERVER_IMAGE + musicIdx)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(album_cover);

            music_name.setText(ret_music_name);
            music_singer.setText(ret_music_singer);
            album_name.setText(ret_album_name);
        }
    }

    @Override
    public void onClick(View v) {
        (new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    String param = "id=" + Develop.getUserId(MusicResultActivity.this) + "&longitude=" + longitude + "&latitude=" + latitude + "&musicidx=" + musicIdx;
                    URL u = new URL(Develop.WEB_SERVER + Develop.WEB_SERVER_UPLOAD);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
                    // 방식결정
                    httpURLConnection.setRequestMethod("POST");
                    // 해더 설정
                    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    // 서버 메세지 수신
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setUseCaches(false);
                    httpURLConnection.setDefaultUseCaches(false);

                    // paramter 전달
                    OutputStream os = httpURLConnection.getOutputStream();
                    os.write(param.getBytes());
                    os.flush();
                    os.close();

                    String buffer;
                    BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                    while (((buffer = in.readLine()) != null)) {
//            Log.d("Test", buffer);
                    }
                    in.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                finish();
            }
        }).execute();
    }
}
