package org.secsm.imhm.Music;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.secsm.imhm.R;

public class MusicRecordFragment extends Fragment {

    private static MusicRecordFragment instance = new MusicRecordFragment();

    public static MusicRecordFragment newInstance() {
        return instance;
    }

    public MusicRecordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.content_main, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.image_btn);
        imageView.setOnClickListener(new MicRecorder());

        return view;
    }

    public class MicRecorder implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // TODO 녹음 화면으로 이동
            Intent i = new Intent(getContext(), RecordingActivity.class);
//      i.putExtra("musicIdx", 43);
//      GpsInfo gps = new GpsInfo(getContext());
//       GPS 사용유무 가져오기
//      if (gps.isGetLocation()) {
//        i.putExtra("latitude", gps.getLatitude());
//        i.putExtra("longitude", gps.getLongitude());
//      }
            startActivity(i);
        }
    }
}