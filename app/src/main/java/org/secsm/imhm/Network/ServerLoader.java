package org.secsm.imhm.Network;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.secsm.imhm.DataSet;
import org.secsm.imhm.Develop;
import org.secsm.imhm.History.MusicCardObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerLoader extends AsyncTask<Void, Void, Void> {

    private Context context;
    private DataSet dataSet;

    public ServerLoader(Context ctx) {
        this.context = ctx;
        this.dataSet = DataSet.newInstance();
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            URL u = new URL(Develop.WEB_SERVER + Develop.WEB_SERVER_HISTORY);
            String param = "id=" + Develop.getUserId(context);
            HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
            // 방식결정
            httpURLConnection.setRequestMethod("POST");
            // 해더 설정
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // 서버 메세지 수신
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDefaultUseCaches(false);

            // paramter 전달
            OutputStream os = httpURLConnection.getOutputStream();
            os.write(param.getBytes());
            os.flush();
            os.close();

            String buffer;
            String ret = "";
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            JSONObject json = null;

            while (((buffer = in.readLine()) != null)) {
                ret += buffer;
            }
            in.close();

            JSONObject jsonObject = new JSONObject(ret);
            int size = jsonObject.getInt("arraysize");

            JSONArray arr = jsonObject.getJSONArray("result");

            for (int i = 0; i < size; i++) {
                dataSet.addItem(convertMusicCardObject(arr.getJSONObject(i)));
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    private MusicCardObject convertMusicCardObject(JSONObject obj) throws JSONException {
        int album_cover = obj.getInt("musicIdx");
        String music_name = obj.getString("title");
        String album_name = obj.getString("albumname");
        String music_singer = obj.getString("artist");
        double latitude = obj.getDouble("latitude");
        double longitude = obj.getDouble("longitude");

        return new MusicCardObject(album_cover, music_name, music_singer, album_name, latitude, longitude);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dataSet.clear();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
