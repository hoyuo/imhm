package org.secsm.imhm.Network;

import android.content.Context;
import android.os.AsyncTask;

import org.secsm.imhm.Develop;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UpdateUUID extends AsyncTask<Void, Void, Void> {

    private Context context;

    public UpdateUUID(Context ctx) {
        this.context = ctx;
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            URL u = new URL(Develop.WEB_SERVER + Develop.WEB_SERVER_UUID);
            String param = "id=" + Develop.getUserId(context) + "&uuid=" + Develop.getUUID(context);
            HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
            // 방식결정
            httpURLConnection.setRequestMethod("POST");
            // 해더 설정
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // 서버 메세지 수신
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDefaultUseCaches(false);

            // paramter 전달
            OutputStream os = httpURLConnection.getOutputStream();
            os.write(param.getBytes());
            os.flush();
            os.close();

            String buffer;
            String ret = "";
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            while (((buffer = in.readLine()) != null)) {
                ret += buffer;
            }
            in.close();

        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

}